var express = require('express');
var bodyParser = require('body-parser'),
    morgan = require('morgan'),
    cors = require('cors');
var shortid = require('shortid');
var uid = require('rand-token').uid;
const lowdb = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');
const adapter = new FileAsync('./src/db/database.json');

var app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

lowdb(adapter)
    .then(db => {
        //Routes
        //GET /api/contacts
        app.get('/api/contacts', function(req, res) {
            let allContacts = db.get(`contacts`).value();
            console.log(allContacts);
            res.json({
                data: allContacts
            });
        });

        //GET /api/contact
        app.get('/api/contact/:id', (req, res) => {
            let contactId = req.params.id;
            console.log(contactId);
            let contact = db.get('contacts').find({ id: contactId }).value();

            res.status(200).json({
                contact: contact
            });
        });

        //POST /api/contact
        app.post('/api/contact', (req, res) => {
            let data = req.body;
            let {name, dob, email, address, phone} = data;
            console.log(data);
            let newContact = {
                name: name,
                dob: dob,
                email: email,
                address: address,
                phone: phone,
                id: shortid.generate(),
                secret: uid(16)
            }
            let result = db.get('contacts')
                .push(newContact)
                .write();
            res.status(201).json({
                result: result
            });
        });

        //PATCH /api/contact
        app.patch('/api/contact', (req, res) => {
            let contactEntry = req.body;
            console.log(`Update contact at id=${contactEntry.id}`, contactEntry);
            let contact = db.get('contacts').find({ id: contactEntry.id }).value();
            console.log(`Requested contact: `, contact);
            if (contact) {
                db.get('contacts').find({ id: contactEntry.id }).assign(
                    {
                        ...contactEntry
                    }
                ).write();
                res.json({
                    result: db.get('contacts').find({ id: contactEntry.id }).value()
                });
            } else {
                res.status(404).json({
                    result: 'Error - Not Found'
                });
            }
        });

        //DELETE /api/contact
        app.delete('/api/contact', (req, res) => {
            let contactId = req.body.id;
            console.log(`Update contact at id=${contactId}`);
            let contact = db.get('contacts').find({ id: contactId }).value();
            if (contact) {
                db.get('contacts').remove({ id: contactId }).write();
                res.json({
                    result: contactId
                });
            } else {
                res.status(404).json({
                    result: 'Error - Not found'
                });
            }
        });
    }).then(() => {
        app.listen(PORT, () => {
            console.log(`Contact Express server is running on ${PORT}`)
        });
    });

const PORT = process.env.PORT || 3030;
